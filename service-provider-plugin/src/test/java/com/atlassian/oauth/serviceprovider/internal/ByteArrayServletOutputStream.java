package com.atlassian.oauth.serviceprovider.internal;

import javax.servlet.ServletOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import static com.google.common.base.Preconditions.checkNotNull;

public final class ByteArrayServletOutputStream extends ServletOutputStream {
    private final OutputStream os;

    public ByteArrayServletOutputStream(ByteArrayOutputStream os) {
        this.os = checkNotNull(os);
    }

    @Override
    public void write(int b) throws IOException {
        os.write(b);
    }
}