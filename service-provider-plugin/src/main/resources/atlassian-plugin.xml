<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" pluginsVersion="2" system="true">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}"/>
    </plugin-info>

    <!-- SPI imports -->
    <component-import key="delegateTokenStore" interface="com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore" />
    <component-import key="consumerStore" interface="com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore" />

    <component-import key="oAuthRequestVerifierFactory" interface="com.atlassian.sal.api.auth.OAuthRequestVerifierFactory" />

    <!-- SAL imports -->
    <component-import key="authenticationController" interface="com.atlassian.sal.api.auth.AuthenticationController" />
    <component-import key="authenticationListener" interface="com.atlassian.sal.api.auth.AuthenticationListener" />
    <component-import key="loginUriProvider" interface="com.atlassian.sal.api.auth.LoginUriProvider" />
    <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties" />
    <component-import key="userManager" interface="com.atlassian.sal.api.user.UserManager" />
    <component-import key="localeResolver" interface="com.atlassian.sal.api.message.LocaleResolver" />
    <component-import key="i18nResolver" interface="com.atlassian.sal.api.message.I18nResolver" />
    <component-import key="transactionTemplate" interface="com.atlassian.sal.api.transaction.TransactionTemplate" />
    <component-import key="pluginScheduler" interface="com.atlassian.sal.api.scheduling.PluginScheduler" />
    <component-import key="requestFactory" interface="com.atlassian.sal.api.net.RequestFactory" />

    <!-- Template renderer import and context items-->
    <component-import key="templateRenderer" interface="com.atlassian.templaterenderer.velocity.one.six.VelocityTemplateRenderer" />

    <component key="helpLinkResolver" class="com.atlassian.oauth.shared.servlet.HelpLinkResolver" />
    <template-context-item key="helpLinkResolverContextItem" component-ref="helpLinkResolver"
        context-key="helpLinkResolver" name="HelpLinkResolver Context Item"/>

    <component key="unescaper" class="com.atlassian.oauth.shared.servlet.Unescaper" />
    <template-context-item key="unescaperContextItem" component-ref="unescaper"
        context-key="unescape" name="Unescaper Context Item"/>

    <component key="stringEscapeUtil" class="org.apache.commons.lang.StringEscapeUtils" />
    <template-context-item key="stringEscapeUtilContextItem" component-ref="stringEscapeUtil"
        context-key="stringEscapeUtil" name="StringEscapeUtil Context Item"/>

    <template-context-item key="applicationPropertiesContextItem" component-ref="applicationProperties"
        context-key="applicationProperties" name="ApplicationProperties Context Item" />

    <!-- Internal plugin components -->
    <component key="randomizer" class="com.atlassian.oauth.serviceprovider.internal.RandomizerImpl">
        <interface>com.atlassian.oauth.serviceprovider.internal.Randomizer</interface>
    </component>
    <component key="tokenFactory" class="com.atlassian.oauth.serviceprovider.internal.TokenFactoryImpl">
        <interface>com.atlassian.oauth.serviceprovider.internal.TokenFactory</interface>
    </component>
    <component key="authenticator" class="com.atlassian.oauth.serviceprovider.internal.AuthenticatorImpl">
        <interface>com.atlassian.oauth.serviceprovider.Authenticator</interface>
    </component>
    <component key="tokenStore" class="com.atlassian.oauth.serviceprovider.internal.DelegatingTokenStoreImpl">
        <interface>com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore</interface>
    </component>
    <component key="clock" class="com.atlassian.oauth.serviceprovider.SystemClock">
        <interface>com.atlassian.oauth.serviceprovider.Clock</interface>
    </component>

    <component key="serviceProviderFactory" class="com.atlassian.oauth.serviceprovider.internal.ServiceProviderFactoryImpl" />
    <component key="validator" class="com.atlassian.oauth.serviceprovider.internal.OAuthValidatorImpl" />
    <component key="converter" class="com.atlassian.oauth.serviceprovider.internal.OAuthConverter" />
    <component key="basicConsumerInformationRenderer" class="com.atlassian.oauth.serviceprovider.internal.servlet.authorize.BasicConsumerInformationRenderer" />

    <component key="compatibilityPluginScheduler" class="com.atlassian.scheduler.compat.AutoDetectingCompatibilityPluginScheduler"/>
    <component key="expiredTokenRemoverScheduler" class="com.atlassian.oauth.serviceprovider.internal.ExpiredSessionRemoverScheduler" public="true">
        <interface>com.atlassian.sal.api.lifecycle.LifecycleAware</interface>
    </component>

    <!--
        AuthorizationRenderer and dynamic imports for ConsumerInformationRenderer are declared in
        META-INF/spring/authorization-renderer.xml.
        AggregateTokenPropertiesFactory and dynamic imports for TokenPropertiesFactorys are declared in
        META-INF/spring/token-properties-factory.xml.
    -->

    <component key="getAuthorizationProcessor" class="com.atlassian.oauth.serviceprovider.internal.servlet.authorize.GetAuthorizationPage" />
    <component key="postAuthorizationProcessor" class="com.atlassian.oauth.serviceprovider.internal.servlet.authorize.PostAuthorization" />
    <component key="tokenLoader" class="com.atlassian.oauth.serviceprovider.internal.servlet.TokenLoaderImpl" />
    <component key="loginRedirector" class="com.atlassian.oauth.serviceprovider.internal.servlet.authorize.LoginRedirectorImpl" />

    <!-- OAuth servlets and filter -->
    <servlet key="oauthRequestTokenServlet" name="OAuth Request Token Servlet" class="com.atlassian.oauth.serviceprovider.internal.servlet.RequestTokenServlet">
        <url-pattern>/oauth/request-token</url-pattern>
    </servlet>

    <servlet key="oauthAccessTokenServlet" name="OAuth Access Token Servlet" class="com.atlassian.oauth.serviceprovider.internal.servlet.AccessTokenServlet">
        <url-pattern>/oauth/access-token</url-pattern>
    </servlet>

    <servlet key="oauthAuthorizeServlet" name="OAuth Authorize Servlet" class="com.atlassian.oauth.serviceprovider.internal.servlet.authorize.AuthorizeServlet">
        <url-pattern>/oauth/authorize</url-pattern>
    </servlet>

    <!-- the weight needs to be suitably large that it should be the last thing that happens before the login filter -->
    <servlet-filter key="oauthFilter" name="OAuth Filter" class="com.atlassian.oauth.serviceprovider.internal.servlet.OAuthFilter"
            location="before-login" weight="1000000">
        <url-pattern>/*</url-pattern>
        <dispatcher>REQUEST</dispatcher>
        <dispatcher>FORWARD</dispatcher>
    </servlet-filter>

    <servlet key="accessTokensServlet" class="com.atlassian.oauth.serviceprovider.internal.servlet.user.AccessTokensServlet" application="jira,confluence,bamboo,fecru">
        <url-pattern>/oauth/users/access-tokens</url-pattern>
    </servlet>

    <servlet key="accessTokensAdgServlet" class="com.atlassian.oauth.serviceprovider.internal.servlet.user.AccessTokensUserProfileServlet" application="bitbucket,refapp">
        <url-pattern>/oauth/users/access-tokens</url-pattern>
    </servlet>

    <resource type="i18n" name="i18n" location="com.atlassian.oauth.serviceprovider.internal.i18n" />
    <resource type="i18n" name="shared-i18n" location="com.atlassian.oauth.shared.servlet.i18n" />

    <web-resource name="Service Provider Authorization Web Resources" key="authorization-resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <resource type="download" name="css/authorize.css" location="css/authorize.css" />
        <resource type="download" name="js/users/authorize.js" location="js/users/authorize.js" />
    </web-resource>

    <web-resource name="Service Provider Access Token Resources" key="access-token-resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <dependency>com.atlassian.auiplugin:aui-experimental-lozenge</dependency>
        <resource type="download" name="css/access-tokens.css" location="css/access-tokens.css" />
        <resource type="download" name="js/users/access-tokens.js" location="js/users/access-tokens.js" />
        <resource type="download" name="images/default-icon.png" location="images/default-icon.png" />
    </web-resource>

    <web-item name="Bitbucket OAuth tokens account nav item" key="oauth-tokens-nav" application="bitbucket" weight="65" section="bitbucket.user.account.nav">
        <label key="bitbucket.web.user.account.tabs.oauth">Authorized applications</label>
        <link>/plugins/servlet/oauth/users/access-tokens</link>
    </web-item>
</atlassian-plugin>
