package com.atlassian.oauth.serviceprovider.internal.servlet.user;

import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.internal.servlet.authorize.LoginRedirector;
import com.atlassian.sal.api.message.LocaleResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.springframework.beans.factory.annotation.Qualifier;


@SuppressWarnings("serial")
public class AccessTokensUserProfileServlet extends AccessTokensServlet {
    public AccessTokensUserProfileServlet(@Qualifier("tokenStore") ServiceProviderTokenStore store,
                                          UserManager userManager,
                                          LocaleResolver localeResolver,
                                          LoginRedirector loginRedirector,
                                          TemplateRenderer templateRenderer) {
        super(store, userManager, localeResolver, loginRedirector, templateRenderer);
    }

    @Override
    protected String getTemplate() {
        return "templates/user/access-tokens-userprofile.vm";
    }
}
