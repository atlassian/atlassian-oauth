/*
 * Copyright 2008 Web Cohesion
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.oauth.serviceprovider.internal;

import com.google.common.base.Preconditions;
import net.oauth.OAuth;
import net.oauth.OAuthProblemException;

import java.util.Iterator;
import java.util.TreeSet;

/**
 * Memory-based nonce service based on
 * <a href="https://fisheye.springsource.org/browse/spring-security-oauth/spring-security-oauth/src/main/java/org/springframework/security/oauth/provider/nonce/InMemoryNonceServices.java?r=d8ac98c84159e2ee0d1a68960a59f20c8600a4a5">org.springframework.security.oauth.provider.nonce.OAuthNonceServices from Spring Security</a>
 * The code is kept as close as possible to the original for easy comparison.
 *
 * InMemoryNonceService first validates the consumer key and timestamp. Assuming the consumer and timestamp are valid,
 * the InMemoryNonceServices further ensures that the specified nonce was not used with the specified timestamp within
 * the specified validity window. The nonces used within the validity window is kept in memory. There is a per request
 * memory overhead. Keeping the validity window short helps prevent wasting too much memory.
 */
public class InMemoryNonceService {

    /**
     * Contains all the nonces that were used inside the validity window.
     */
    static final TreeSet<NonceEntry> NONCES = new TreeSet<NonceEntry>();

    private volatile long lastCleaned = 0;

    // this should not be too large, otherwise the amount of memory used on NONCES can get quite large.
    private long validityWindowSeconds;

    public InMemoryNonceService(long validityWindowSeconds) {
        this.validityWindowSeconds = validityWindowSeconds;
    }

    /**
     * Validates the nonce.
     *
     * @param consumerKey consumer key
     * @param timestamp   timestamp as the number of seconds since January 1, 1970 00:00:00 GMT (according to OAuth 1.0 spec)
     * @param nonce       nonce
     * @throws OAuthProblemException if the nonce has been used.
     */
    public void validateNonce(String consumerKey, long timestamp, String nonce) throws OAuthProblemException {

        Preconditions.checkNotNull(consumerKey);
        Preconditions.checkNotNull(nonce);

        NonceEntry entry = new NonceEntry(consumerKey, timestamp, nonce);

        synchronized (NONCES) {
            if (NONCES.contains(entry)) {
                throw new OAuthProblemException(OAuth.Problems.NONCE_USED);
            } else {
                NONCES.add(entry);
            }
            cleanupNonces();
        }
    }

    private void cleanupNonces() {
        long now = System.currentTimeMillis() / 1000;
        // don't clean out the NONCES for each request, this would cause the service to be constantly locked on this
        // loop under load. One second is small enough that cleaning up does not become too expensive.
        // Also see SECOAUTH-180 for reasons this class was refactored.
        if (now - lastCleaned > 1) {
            Iterator<NonceEntry> iterator = NONCES.iterator();
            while (iterator.hasNext()) {
                // the nonces are already sorted, so simply iterate and remove until the first nonce within the validity
                // window.
                NonceEntry nextNonce = iterator.next();
                long difference = now - nextNonce.timestamp;
                if (difference > getValidityWindowSeconds()) {
                    iterator.remove();
                } else {
                    break;
                }
            }
            // keep track of when cleanupNonces last ran
            lastCleaned = now;
        }
    }

    /**
     * Set the timestamp validity window (in seconds).
     *
     * @return the timestamp validity window (in seconds).
     */
    public long getValidityWindowSeconds() {
        return validityWindowSeconds;
    }

    /**
     * The timestamp validity window (in seconds).
     *
     * @param validityWindowSeconds the timestamp validity window (in seconds).
     */
    public void setValidityWindowSeconds(long validityWindowSeconds) {
        this.validityWindowSeconds = validityWindowSeconds;
    }

    /**
     * Representation of a nonce with the right hashCode, equals, and compareTo methods for the TreeSet approach above
     * to work.
     */
    static class NonceEntry implements Comparable<NonceEntry> {
        private final String consumerKey;

        private final long timestamp;

        private final String nonce;

        public NonceEntry(String consumerKey, long timestamp, String nonce) {
            this.consumerKey = consumerKey;
            this.timestamp = timestamp;
            this.nonce = nonce;
        }

        @Override
        public int hashCode() {
            return consumerKey.hashCode() * nonce.hashCode() * Long.valueOf(timestamp).hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof NonceEntry)) {
                return false;
            }
            NonceEntry arg = (NonceEntry) obj;
            return timestamp == arg.timestamp && consumerKey.equals(arg.consumerKey) && nonce.equals(arg.nonce);
        }

        public int compareTo(NonceEntry o) {
            // sort by timestamp
            if (timestamp < o.timestamp) {
                return -1;
            } else if (timestamp == o.timestamp) {
                int consumerKeyCompare = consumerKey.compareTo(o.consumerKey);
                if (consumerKeyCompare == 0) {
                    return nonce.compareTo(o.nonce);
                } else {
                    return consumerKeyCompare;
                }
            } else {
                return 1;
            }
        }

        @Override
        public String toString() {
            return timestamp + " " + consumerKey + " " + nonce;
        }
    }
}
