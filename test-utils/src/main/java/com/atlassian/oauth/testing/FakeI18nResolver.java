package com.atlassian.oauth.testing;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.message.MessageCollection;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

public class FakeI18nResolver implements I18nResolver {
    public String getText(String key) {
        return key;
    }

    @Override
    public String getText(Locale locale, String key) {
        return key;
    }

    public Map<String, String> getAllTranslationsForPrefix(String prefix) {
        return null;
    }

    public String getText(Message message) {
        return message.getKey();
    }

    @Override
    public String getText(Locale locale, Message message) {
        return message.getKey();
    }

    @Override
    public String getRawText(String s) {
        return null;
    }

    @Override
    public String getRawText(final Locale locale, final String key) {
        return null;
    }

    public String getText(String key, Serializable... arguments) {
        return key;
    }

    @Override
    public String getText(Locale locale, String key, Serializable... serializables) {
        return key;
    }

    public Message createMessage(final String key, final Serializable... arguments) {
        return new Message() {
            public Serializable[] getArguments() {
                return arguments;
            }

            public String getKey() {
                return key;
            }
        };
    }

    public MessageCollection createMessageCollection() {
        return null;
    }

    public Map<String, String> getAllTranslationsForPrefix(String prefix, Locale locale) {
        return null;
    }
}